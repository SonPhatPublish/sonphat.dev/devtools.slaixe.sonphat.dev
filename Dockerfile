FROM nginx:1.21.1 AS builder

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /wwwroot
COPY /public/ .


FROM builder AS runtime
EXPOSE 80